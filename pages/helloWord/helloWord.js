// pages/helloWord/helloWord.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    searchContent: "",
    bookList: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.searchBook();
  },
  searchBook(e) {
    console.log(this.data.searchContent);
    var title = this.data.searchContent;
    var that = this;
    // 重置数据 
    that.setData({
      bookList: null
    }) 
    wx.request({
      url: 'https://6016a95m81.zicp.fun/v1/weChat/findAllByTitle',
      method: "GET",
      data: {
        title: title
      },
      header: {
        'content-type': 'application/json' // 默认值 
      },
      success(res) {
        console.log(res.data);
        var bookList = res.data;
        console.log(that) 
        that.setData({
          bookList: bookList
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})